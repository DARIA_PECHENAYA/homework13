'use strict';
 
import gulp from 'gulp';
import sass from 'gulp-sass';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import compiler from  'node-sass';
import rigger from 'gulp-rigger';
import imagemin from 'gulp-imagemin';
 

sass.compiler = compiler;
 
gulp.task('sass', function () {
  return gulp.src('./src/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('./build/css'));
});

gulp.task('js', function() {
  return gulp.src('./src/js/**/*.js')
        .pipe(rigger())
        .pipe(concat('index.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build/js'));
});

gulp.task('images', function() {
    return gulp.src('./src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./build/images'))
})

gulp.task('build-all', gulp.series('sass', 'js', 'images'));

// gulp.task('sass:watch', function () {
//   gulp.watch('./src/sass/**/*.scss', ['sass']);
// });